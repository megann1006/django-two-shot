from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.


def receipts(request):
    if request.user.is_authenticated:
        receipts = Receipt.objects.filter(purchaser=request.user)
        context = {
            "receipts": receipts,
        }
        return render(request, "receipts/list.html", context)
    else:
        return redirect('login')


@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipts = form.save(commit=False)
            receipts.purchaser = request.user
            receipts.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_receipt.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "categories/list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "accounts/list.html", context)


@login_required
def create_category(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
